sbtVersion := "0.12.3"

name := "poc_stm"

organization := "binarytemple"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.0"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

resolvers += "Spray Repository" at "http://repo.spray.io/"

autoCompilerPlugins := true

//Scala STM

libraryDependencies += "org.scala-stm" %% "scala-stm" % "0.7"

//Classic Actors, Typed Actors, IO Actor etc.
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.1.2"

// Remote Actors
libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.1.2"

// Toolkit for testing Actor systems
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.1.2"

// Akka microkernel for running a bare-bones mini application server
libraryDependencies += "com.typesafe.akka" %% "akka-kernel" % "2.1.2"

// Transactors - transactional actors, integrated with Scala STM
libraryDependencies += "com.typesafe.akka" %% "akka-transactor" % "2.1.2"

//Agents, integrated with Scala STM
libraryDependencies += "com.typesafe.akka" %% "akka-agent" % "2.1.2"

//Dataflow plugin.
libraryDependencies += "com.typesafe.akka" %% "akka-dataflow" % "2.1.2"

//Dataflow plugin.
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.1.2" % "test"

//SLF4J Event Handler Listener
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.1.2"

libraryDependencies += "org.apache.derby" % "derby" % "10.4.1.3"

libraryDependencies +=  "junit" % "junit" % "4.9"  % "test"

libraryDependencies +=  "org.slf4j" % "slf4j-simple" % "1.5.6"

libraryDependencies += "commons-logging" % "commons-logging" % "1.1"

libraryDependencies += "commons-collections" % "commons-collections" % "3.2.1"

libraryDependencies += "commons-configuration" % "commons-configuration" % "1.6"

libraryDependencies += "commons-io" % "commons-io" % "2.4"

libraryDependencies += "log4j" % "log4j" % "1.2.16"

libraryDependencies += "c3p0" % "c3p0" % "0.9.1"

libraryDependencies += "commons-lang" % "commons-lang" % "2.5"

libraryDependencies += "commons-net" % "commons-net" % "3.0.1"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.6"

libraryDependencies += "com.novocode" % "junit-interface" % "0.10-M2" % "test"

libraryDependencies += "com.novocode" % "junit-interface" % "0.10-M2"

libraryDependencies += "joda-time" % "joda-time" % "1.6.2"

libraryDependencies += "org.specs2" %% "specs2" % "1.13" % "test"

libraryDependencies += "org.mockito" % "mockito-all" % "1.9.0" % "test"

libraryDependencies += "org.jsoup" % "jsoup" % "1.7.2" % "test"

libraryDependencies += "io.spray" %  "spray-json_2.10" % "1.2.3" 

libraryDependencies += "com.codahale" % "jerkson_2.9.1" % "0.5.0"

libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.10.0"

EclipseKeys.withSource := true

net.virtualvoid.sbt.graph.Plugin.graphSettings
