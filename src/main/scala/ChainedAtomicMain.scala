
/**
 * This example demonstrates jumping to an alternate atomic block.
 */
object ChainedAtomicMain {


  def main(args: Array[String]) {
    import scala.concurrent.stm._

    val counter = Ref(0)
    val counter2 = Ref(0)

    atomic {
      implicit txn =>
        counter() = 1
        if (true) {
          //Retry will jump to chained block
          retry
        }
    } orAtomic {
      implicit txn =>
        counter() = 10
        counter2() = 10
    }
    println(s"Expecting (counter,counter2) == (10,10)")
    println(s"Value == ( ${counter.single()},${counter2.single()}  )")
  }
}
