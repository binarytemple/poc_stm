import scala.concurrent.stm._

/**
 * This example demonstrates something akin to a blocking concurrent queue.
 *
 * Insert values into a TSet from one Thread, then using retryFor inside an atomic block,
 * effectively polling that TSet until a value becomes available.
  */
object SetMain {
  def main(args: Array[String]) {

    val a = TSet[Int]()
    val count = Ref(0)

    val t = new Thread {
      override def run() {
        println("running!")
        while (count.single() < 3) {
          atomic {
            implicit txn =>
              if (!(a.size > 0)) {
                println("retrying")
                retryFor(100)
              } else {
                if (count.getAndTransform(_ + 1) < 4) {
                  a.remove(0)
                  println("removed")
                }
              }
          }
        }
      }
    }
    t.setDaemon(false)
    t.start()
    atomic {
      implicit txn =>
        a.add(1)
        Thread.sleep(100)
        a.add(2)
        Thread.sleep(100)
        a.add(3)
        Thread.sleep(100)
        println("finished adding")
    }
    Thread.sleep(2000)
    println(s"Count == ${count.single()}")
  }
}
