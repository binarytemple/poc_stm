import akka.actor.{Cancellable, Props, Actor, ActorSystem}
import akka.routing.RandomRouter
import java.util.concurrent.{TimeUnit, CountDownLatch}
import scala.concurrent.stm._


/**
 * This example demonstrates one Actor updating values in a shared Map,
 * and another Actor viewing those values.
 */
object MapPuttingAndShowingMain {

  class PutActor(t: TMap[Int, String], completed: Ref[Boolean]) extends Actor {
    def receive = {
      case update@((k: Int, v: String)) => atomic {
        implicit txn =>
          println(s" ${self.path} trying to update TMap with $update")
          t.put(k, v)
          if (t.size == 9) {
            completed.set(true)
          }
      }
    }
  }

  class ShowActor(t: TMap[Int, String]) extends Actor {
    def receive = {
      case msg => atomic {
        implicit txn =>
          println(s" ${self.path} : received $msg , displaying  ${t.single}")
      }
    }
  }

  def main(args: Array[String]) {
    val as = ActorSystem("db-as")

    val map = TMap[Int, String]()

    val completed = Ref(false)
    val showActor = as.actorOf(Props[ShowActor].withCreator(new ShowActor(map)))

    val router1 = as.actorOf(Props[PutActor].withCreator(new PutActor(map, completed)).withRouter(RandomRouter(nrOfInstances = 5)))

    Range(1, 10).map(x => (x, "v-" + x.toString)).foreach(x => {
      router1 ! x
    })

    do {
      Thread.sleep(500)
    } while (completed.single.get == false)

    println("asking for show!")

    showActor ! 'show

    val cdl = new CountDownLatch(1)
    cdl.await(2, TimeUnit.SECONDS)
    as.shutdown()
  }
}
