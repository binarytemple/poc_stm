import scala.concurrent.stm._

/**
 * This example demonstrates coordinated update of array elements.
 */
object ArrayMain {
  def main(args: Array[String]) {

    val a = TArray(Array(1, 2, 3))
    atomic {
      implicit txn =>
        a.update(0, 0)
        a.update(1, 1)
        a.update(2, 2)
    }
    println(a.single.tarray)
  }
}
