import scala.concurrent.stm._

object StringTxnMain {


  /**
   * Example of setting a Ref[String] using nested transactions, the innermost of which will fail.
   */
  def setStringInNestedTransactions {
    println("- setStringInNestedTransactions -")
    val last = Ref("nothing")
    atomic {
      implicit txn =>
        last() = "set by outer transaction"
        try {
          atomic {
            implicit txn =>
              last() = "set by inner transaction"
              throw new RuntimeException
          }
        } catch {
          case _: RuntimeException => println("Got runtime exception. Rolled back the Ref[String], it should now == 'set by outer transaction' ")
        }
    }

    println(s" Ref[String] == ${last.single()}")
  }

  def main(args: Array[String]) {
    setStringInNestedTransactions
  }
}
