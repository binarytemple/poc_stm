import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.stm._

object IncrementIntRecoverMain {

  /**
   * Attempt to increment an integer within nested transactions, fail - but recover, and retry the transaction for up to 1 second.
   */
  def incrementIntRecoverFromFailedTransaction {
    println("- incrementIntRecoverFromFailedTransaction -")
    val firstRun = new AtomicBoolean(true)
    val last = Ref(1)
    atomic {
      implicit txn =>
        last() = last() + 1
        try {
          atomic {
            implicit txn =>
            //Never gets incremented here
              last() = last() + 1
              //Throw Exception the first time this is run
              if (firstRun.getAndSet(false) == true) {
                throw new RuntimeException
              }
          }
        } catch {
          case _: RuntimeException => println("Got runtime exception, attempting a retry"); retryFor(1000)
        }
    }
    atomic {
      implicit txn =>
        println(s"last should be = 3 , last = ${last.get}")
    }
  }

  def main(args: Array[String]) {
    incrementIntRecoverFromFailedTransaction
  }
}
