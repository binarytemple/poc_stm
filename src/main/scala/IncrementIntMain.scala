import scala.concurrent.stm._

/**
 * An example of incrementing a value, and handling an Exception.
 */
object IncrementIntMain {

  /**
   * Increment a Ref[Integer] inside nested transactions, the innermost of which will fail
   */
  def incrementIntWithinNestedTransactions {
    println("- incrementIntWithinNestedTransactions -")
    val ref = Ref(1)
    atomic {
      implicit txn =>
        ref() = ref() + 1
        try {
          atomic {
            implicit txn =>
              ref() = ref() + 1
              throw new RuntimeException
          }
        } catch {
          case _: RuntimeException => println("Got runtime exception, rolled back the Ref[Int], it should now == 2")
        }
    }
    atomic {
      implicit txn =>
        println(s"ref should == 2 , ref == ${ref.get}")
    }
  }

  def main(args: Array[String]) {
    incrementIntWithinNestedTransactions
  }
}
